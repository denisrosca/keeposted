## KeePosted ##

[![Build Status](https://api.shippable.com/projects/5546767eedd7f2c052de2ccf/badge?branchName=master)](https://app.shippable.com/projects/5546767eedd7f2c052de2ccf/builds/latest)

KeePosted aims to be a batch notification service that will periodically poll notification sources and build a list of outstanding notifications.

### Use Case ###

I don't have notifications for email/social media because I don't want the interruption but, during downtime, I find myself checking through all the different services I use to see what's outstanding. I would like to have a service that would ping all my communication channels periodically(every 15/30/60/90/configurable minutes or on request) and give me a summary on a page or to a mobile app. I don't need the app to interact with these services, just ping via API and return what's "unread."

### Building the app ###

In order to build the application you have to install [SBT](http://www.scala-sbt.org).
To run the unit tests: 
```
#!scala

sbt clean test
```