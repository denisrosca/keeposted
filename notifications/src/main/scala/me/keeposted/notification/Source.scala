package me.keeposted.notification

sealed trait Source {
  def name:String
}

case object Facebook extends Source {
  def name = "Facebook"
}

case object Twitter extends Source {
  def name = "Twitter"
}
