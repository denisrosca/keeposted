package me.keeposted.notification.retrieve.facebook

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import org.scalactic._
import com.typesafe.scalalogging.LazyLogging

import akka.actor.ActorSystem
import spray.client.pipelining._

import me.keeposted.common.user.UserId
import me.keeposted.notification.retrieve.NotificationRetriever
import me.keeposted.notification.{Facebook, Notification, Source}
import me.keeposted.notification.retrieve.facebook.NotificationsUnmarshaller._

class FacebookNotificationRetriever(config: FacebookConfiguration,
                                    tokenManager: FacebookTokenManager,
                                    uriBuilder: UriBuilder)
  extends NotificationRetriever
  with EndpointConsumer[List[Notification] Or FacebookError]
  with LazyLogging {

  protected implicit val actorSystem = ActorSystem()

  override def supportedChannel: Source = Facebook

  override def getNewNotifications(id: UserId): Future[List[Notification] Or FacebookError] = {
    tokenManager.getToken(id) flatMap {
      case Good(Some(token)) => invokeApiEndpoint(token)
      case Good(None) => Future.successful(Bad(MissingToken))
      case Bad(error) => Future.successful(Bad(error))
    }
  }

  private def invokeApiEndpoint(token: LongTermToken): Future[List[Notification] Or FacebookError] = {
    val uri = uriBuilder.build(config.notificationsEndpoint, token)
    pipeline.apply(Get(uri))
  }
}