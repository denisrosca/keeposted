package me.keeposted.notification.retrieve.facebook

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import com.typesafe.scalalogging.LazyLogging

import akka.actor.ActorSystem
import spray.client.pipelining._
import spray.http.HttpHeaders.Accept
import spray.http.{HttpRequest, MediaTypes}

trait EndpointConsumer[T] {
  this: LazyLogging =>

  protected implicit def actorSystem: ActorSystem

  def pipeline(implicit unmarshaller: Unmarshaller[T]): HttpRequest => Future[T] =
    addHeader(Accept(MediaTypes.`application/json`)) ~>
        logRequest(request => logger.debug(request.toString)) ~>
        sendReceive ~>
        logResponse(response => logger.debug(response.toString)) ~>
        unmarshaller.unmarshal

}