package me.keeposted.notification.retrieve.facebook

import com.typesafe.config.{ConfigFactory, Config}

class FacebookConfiguration(config: Config) {

  config.checkValid(ConfigFactory.defaultReference(), "notifications.facebook.host")
  config.checkValid(ConfigFactory.defaultReference(), "notifications.facebook.notifications_endpoint")
  config.checkValid(ConfigFactory.defaultReference(), "notifications.facebook.app_secret")

  def this() {
    this(ConfigFactory.load())
  }

  val host = config.getString("notifications.facebook.host")
  val notificationsEndpoint = config.getString("notifications.facebook.notifications_endpoint")
  val appSecret = config.getString("notifications.facebook.app_secret")
  val debugLevel: Option[String] = {
    val path = "notifications.facebook.debugLevel"
    if(config.hasPath(path))
      Option(config.getString(path))
    else
      None
  }
}
