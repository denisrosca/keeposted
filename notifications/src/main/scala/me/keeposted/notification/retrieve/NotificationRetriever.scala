package me.keeposted.notification.retrieve

import me.keeposted.common.error.ServiceError
import me.keeposted.common.user.UserId
import me.keeposted.notification.{Notification, Source}
import org.scalactic.Or

import scala.concurrent.Future

/** A entity capable of retrieving user notification from a source
  */
trait NotificationRetriever {

  /** The source supported by the specific retriever implementation */
  def supportedChannel: Source

  /** Retrieve all new user notifications
    *
    * @param id of the user for which to retrieve the notifications
    * @return a future containing a set of all notifications or all errors that were encountered
    */
  def getNewNotifications(id: UserId): Future[List[Notification] Or ServiceError]

}