package me.keeposted.notification.retrieve.facebook

sealed trait AccessToken {
  def value:String
}

case class ShortTermToken(value:String) extends AccessToken
case class LongTermToken(value:String) extends AccessToken
