package me.keeposted.notification.retrieve.facebook

import org.scalactic._
import com.typesafe.scalalogging.LazyLogging

import spray.http.StatusCodes._
import spray.http._
import spray.httpx.SprayJsonSupport._
import spray.httpx.unmarshalling._

import me.keeposted.notification.Notification

class NotificationsUnmarshaller
  extends Unmarshaller[List[Notification] Or FacebookError]
  with LazyLogging {

  import FacebookNotificationJsonProtocol._

  override def unmarshal(response: HttpResponse): Or[List[Notification], FacebookError] = {
    if (response.status.isSuccess) {
      response.as[List[Notification] Or DeserializationError] match {
        case Right(value) => value
        case Left(error) => Bad(GeneralError(error.toString))
      }
    } else {
      response.status match {
        case BadRequest => response.as[FacebookError] match {
          case Right(value) => Bad(value)
          case Left(error) => Bad(GeneralError(error.toString))
        }
        case _ =>
          logger.error("Facebook request failed. {}", response)
          Bad(GeneralError("Facebook request failed"))
      }
    }
  }

}

object NotificationsUnmarshaller {
  implicit def unmarshaller = new NotificationsUnmarshaller
}