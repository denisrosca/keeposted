package me.keeposted.notification.retrieve.facebook

import me.keeposted.common.user.UserId
import org.scalactic.Or

import scala.concurrent.Future

trait FacebookTokenManager {

  def getToken(id:UserId): Future[Option[LongTermToken] Or FacebookError]

}