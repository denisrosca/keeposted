package me.keeposted.notification.retrieve.facebook

import spray.http.Uri

import me.keeposted.common.hmac.Hmac

class UriBuilder(config: FacebookConfiguration){

  def build(endpoint: String, token: AccessToken, queryParameters: Map[String, String] = Map()): Uri = {
    val debug: Option[(String, String)] = config.debugLevel match {
      case Some(level) => Some("debug" -> level)
      case None => None
    }

    Uri(endpoint).withQuery(
      Map(
        "access_token" -> token.value,
        "appsecret_proof" -> Hmac.sha256.encode(config.appSecret, token.value)
      ) ++ queryParameters
        ++ debug.toMap
    )
  }
}
