package me.keeposted.notification.retrieve.facebook

import me.keeposted.common.error.ServiceError

sealed trait FacebookError extends ServiceError

case class GeneralError(message:String) extends FacebookError
object GeneralError {
  def apply(t: Throwable):GeneralError = GeneralError(t.getMessage)
}

case object MissingToken extends FacebookError

sealed trait FacebookProtocolError extends FacebookError

sealed trait DeserializationError extends FacebookProtocolError
case class ContentExpected(message: String) extends DeserializationError
case class MalformedContent(message: String) extends DeserializationError

sealed trait OAuthError extends FacebookProtocolError {
  def message: String
  def code: Int
}

case class TokenExpired(message: String, code: Int, subCode: Int) extends OAuthError
case class PermissionDenied(message: String, code: Int) extends OAuthError
case class UnkownError(message: String, code: Int, subCode: Option[Int] = None) extends OAuthError