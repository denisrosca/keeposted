package me.keeposted.notification.retrieve.facebook

import scala.annotation.implicitNotFound

import spray.http.HttpResponse

@implicitNotFound("Implicit unmarshaller not found for type ${T}")
trait Unmarshaller[T] {
  def unmarshal(response: HttpResponse): T
}