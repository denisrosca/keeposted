package me.keeposted.notification.retrieve.facebook

import org.joda.time.DateTime
import org.scalactic.{Bad, Good, Or}
import com.typesafe.scalalogging.LazyLogging

import spray.json._

import me.keeposted.notification.{Facebook, Notification, NotificationId}

object FacebookNotificationJsonProtocol extends DefaultJsonProtocol with LazyLogging {

  type DeserializationResult = List[Notification] Or DeserializationError

  val UNEXPECTED_CONTENT_STRUCTURE = "Content does not match expected structure"
  val MISSING_DATA_FIELD = """Missing "data" field"""
  val MISSING_ERROR_FIELD = """Missing "error" field"""
  val MISSING_ERROR_CODE_FIELD = """Missing "code" field"""
  val OAUTH_EXCEPTION = "OAuthException"

  implicit object FacebookJsonReader extends RootJsonReader[DeserializationResult] {

    implicit val notificationFormat = jsonFormat(notificationFactory, "id", "title", "updated_time")

    override def read(json: JsValue): DeserializationResult = {
      logger.debug("Parsing successful facebook response {}", json.toString())
      json.asJsObject.fields.get("data") match {
        case Some(JsArray(jsonEntries)) =>
          try {
            val notifications = jsonEntries.map {
              jsonEntry => jsonEntry.convertTo[Notification]
            }.toList
            Good(notifications)
          } catch {
            case e: DeserializationException =>
              logger.error("Facebook message deserialization failed with exception", e)
              Bad(MalformedContent(UNEXPECTED_CONTENT_STRUCTURE))
          }
        case _ =>
          Bad(ContentExpected(MISSING_DATA_FIELD))
      }
    }

    def notificationFactory(id: String, title: String, updatedOn: String):Notification =
      Notification(NotificationId(id), title, DateTime.parse(updatedOn), Facebook)
  }

  implicit object FacebookErrorReader extends RootJsonReader[FacebookError] {

    implicit val expiredTokenFormat = jsonFormat(TokenExpired, "message", "code", "error_subcode")

    implicit val permissionDeniedFormat = jsonFormat2(PermissionDenied)

    implicit val unknownErrorFormat = jsonFormat(UnkownError, "message", "code", "error_subcode")

    override def read(json: JsValue): FacebookError = {
      logger.debug("Parsing unsuccessful facebook response {}", json.toString())
      try {
        json.asJsObject.fields.get("error") match {
          case Some(error) =>
            deserializeError(error.asJsObject)
          case _ =>
            ContentExpected(MISSING_ERROR_FIELD)
        }
      } catch {
        case e:DeserializationException =>
          logger.error("Facebook message deserialization failed with exception", e)
          MalformedContent(e.toString)
      }
    }

    def deserializeError(error: JsObject): FacebookError = {
      error.fields.get("code") match {
        case Some(JsNumber(code)) if code.toIntExact == 190 => error.convertTo[TokenExpired]
        case Some(JsNumber(code)) if isPermissionDenied(code.toIntExact) => error.convertTo[PermissionDenied]
        case Some(JsNumber(code)) => error.convertTo[UnkownError]
        case None =>
          MalformedContent(MISSING_ERROR_CODE_FIELD)
      }
    }

    def isPermissionDenied(code: Int) = code >= 200 && code < 300
  }
}