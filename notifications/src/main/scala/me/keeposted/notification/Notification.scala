package me.keeposted.notification

import java.util.UUID

import org.joda.time.DateTime

case class NotificationId(value: String)

object NotificationId {

  def generateNew: NotificationId = NotificationId(UUID.randomUUID().toString)

}

case class Notification(id: NotificationId, value: String, timestamp: DateTime, channel: Source)