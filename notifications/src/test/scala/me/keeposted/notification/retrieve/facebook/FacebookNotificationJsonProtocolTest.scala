package me.keeposted.notification.retrieve.facebook

import org.joda.time.DateTime
import org.scalacheck.Gen
import org.scalactic.{Bad, Good}
import org.scalatest.prop.PropertyChecks
import org.scalatest.{FunSpec, Matchers}

import spray.json._
import me.keeposted.notification.{Facebook, Notification, NotificationId}

class FacebookNotificationJsonProtocolTest extends FunSpec with Matchers with PropertyChecks {

  import FacebookNotificationJsonProtocol._

  describe("The Facebook Notification Protocol") {
    describe("when deserializing a successful response") {
      describe("without any notifications") {
        it("should return an empty notifications list") {
          val json =
            """
              {"data":[]}
            """.parseJson
          json.convertTo[DeserializationResult] shouldBe Good(List())
        }
      }

      describe("with only one notification") {
        it("should return a list containing one notification") {
          val json =
            """{
                    "data":[
                      {
                        "id":"test_id",
                        "title":"test_title",
                        "updated_time":"2015-05-01T18:04:59Z"
                      }
                    ]
                  }"""
              .parseJson
          json.convertTo[DeserializationResult] shouldBe Good(
            List(
              Notification(NotificationId("test_id"), "test_title", DateTime.parse("2015-05-01T18:04:59Z"), Facebook)
            )
          )
        }
      }

      describe("with multiple notifications") {
        it("should return a list containing all deserialized notifications") {
          val json =
            """{
              "data":[
                {
                  "id":"test_id_1",
                  "title":"test_title_1",
                  "updated_time":"2015-05-01T18:04:59Z"
                },
                {
                  "id":"test_id_2",
                  "title":"test_title_2",
                  "updated_time":"2015-05-02T18:04:59Z"
                }
              ]
              }"""
              .parseJson
          json.convertTo[DeserializationResult] shouldBe
            Good(
              List(
                Notification(NotificationId("test_id_1"), "test_title_1", DateTime.parse("2015-05-01T18:04:59Z"), Facebook),
                Notification(NotificationId("test_id_2"), "test_title_2", DateTime.parse("2015-05-02T18:04:59Z"), Facebook)
              )
            )
        }
      }

      describe( """without a "data" field""") {
        it("should return a ContextExpected message") {
          val json = """{}""".parseJson
          json.convertTo[DeserializationResult] shouldBe Bad(ContentExpected(MISSING_DATA_FIELD))
        }
      }

    }

    describe("when deserializing a failed response") {

      describe( """without an "error" field""") {
        it("should return a ContextExpected message") {
          val json = """{}""".parseJson
          json.convertTo[FacebookError] shouldBe ContentExpected(MISSING_ERROR_FIELD)
        }
      }

      describe( """with type=OAuth_Exception and code=190""") {
        it("should return a TokenExpired error") {
          val errorMessage = "Authentication Failed"
          val errorCode = 190
          val errorSubCode = 463
          val json =
            s"""
              {
                "error":{
                  "message":"$errorMessage",
                  "type":"OAuthException",
                  "code": $errorCode,
                  "error_subcode": $errorSubCode
                }
              }
            """.parseJson
          json.convertTo[FacebookError] shouldBe TokenExpired(errorMessage, errorCode, errorSubCode)
        }
      }

      describe( """with type=OAuth_Exception and code between [200; 300)""") {
        it("should return a PermissionDenied error") {
          val errorMessage = "Authentication Failed"
          val codes = for (code <- Gen.chooseNum[Int](200, 299)) yield code
          forAll(codes) {
            code: Int =>
              val json =
                s"""
                  {
                    "error":{
                      "message":"$errorMessage",
                      "type":"OAuthException",
                      "code": $code
                    }
                  }
                """.parseJson
              json.convertTo[FacebookError] shouldBe PermissionDenied(errorMessage, code)
          }
        }
      }

      describe( """with type=OAuth_Exception and an unknown error code""") {
        it("should return a UnkownError object") {
          val errorMessage = "Authentication Failed"
          val errorCode = 100
          val errorSubCode = 957
          val json =
            s"""
              {
                "error":{
                  "message":"$errorMessage",
                  "type":"OAuthException",
                  "code": $errorCode,
                  "error_subcode": $errorSubCode
                }
              }
            """.parseJson
          json.convertTo[FacebookError] shouldBe UnkownError(errorMessage, errorCode, Some(errorSubCode))
        }
      }

    }

    describe("when deserializing a notification") {
      describe("without the id field") {
        it("should return a MalformedContent error") {
          val json =
            """{
                  "data":[{
                    "title":"test title",
                    "updated_time":"2015-05-01T19:47:26+03:30"
                  }]
                }""".parseJson
          json.convertTo[DeserializationResult] shouldBe Bad(MalformedContent(UNEXPECTED_CONTENT_STRUCTURE))
        }
      }

      describe("without the title field") {
        it("should return a MalformedContent error") {
          val json =
            """{
                "data":[{
                "id":"random_id",
                "updated_time":"2015-05-01T19:47:26+03:30"
                }]
                }""".stripMargin.parseJson
          json.convertTo[DeserializationResult] shouldBe Bad(MalformedContent(UNEXPECTED_CONTENT_STRUCTURE))
        }
      }

      describe("without the updated_time field") {
        it("should return a MalformedContent error") {
          val json =
            """{
                "data":[{
                "id":"random_id",
                "title":"test title"
                }]
                }""".stripMargin.parseJson
          json.convertTo[DeserializationResult]
        }
      }
    }
  }
}