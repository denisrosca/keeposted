package me.keeposted.notification.retrieve.facebook

import org.scalatest.{Matchers, FunSpec}

import me.keeposted.common.hmac.Hmac

class UriBuilderTest extends FunSpec with Matchers {

  val config = new FacebookConfiguration()
  val builder = new UriBuilder(config)


  describe("A UriBuilder") {
    describe("when constructing the uri") {
      val tokenValue: String = "test_token"
      val uri = builder.build(config.notificationsEndpoint, LongTermToken(tokenValue))

      it("should add a appsecret_proof parameter to the request") {
        uri.query.get("appsecret_proof") shouldBe Some(Hmac.sha256.encode(config.appSecret, tokenValue))
      }

      it("should add a access_token parameter to the request") {
        uri.query.get("access_token") shouldBe Some(tokenValue)
      }
    }
  }
}