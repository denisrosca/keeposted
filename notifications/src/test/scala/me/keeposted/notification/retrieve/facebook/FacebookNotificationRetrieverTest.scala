package me.keeposted.notification.retrieve.facebook

import scala.concurrent.Future

import org.scalactic.{Bad, Good, Or}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{FunSpec, Matchers}

import me.keeposted.common.user.UserId

class FacebookNotificationRetrieverTest extends FunSpec with Matchers with ScalaFutures {

  val config = new FacebookConfiguration()
  val uriBuilder = new UriBuilder(config)

  describe("A Facebook notification retriever") {

    describe("when retrieving notifications") {

      it("should return a MissingToken error if the user doesn't have a token") {
        val tokenManager = NoTokenFoundManager
        val retriever = new FacebookNotificationRetriever(config,tokenManager, uriBuilder)
        val future = retriever.getNewNotifications(UserId.generateNew)
        whenReady(future) {
          result =>
            result shouldBe Bad(MissingToken)
        }
      }

      it("should propagate the error if token retrieval fails with an error") {
        val tokenManager = FailureTokenManager
        val retriever = new FacebookNotificationRetriever(config,tokenManager, uriBuilder)
        val future = retriever.getNewNotifications(UserId.generateNew)
        whenReady(future) {
          result =>
            result shouldBe Bad(GeneralError("Token retrieval failed"))
        }
      }
    }
  }

  case object FailureTokenManager extends FacebookTokenManager {
    override def getToken(id: UserId): Future[Option[LongTermToken] Or FacebookError] = {
      Future.successful(Bad(GeneralError("Token retrieval failed")))
    }
  }

  case object NoTokenFoundManager extends FacebookTokenManager {
    override def getToken(id: UserId): Future[Option[LongTermToken] Or FacebookError] = {
      Future.successful(Good(None))
    }
  }
}