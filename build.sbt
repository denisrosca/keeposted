name := "KeePosted"

lazy val commonSettings = Seq(
  organization := "me.keeposted",
  version := "0.1.0-SNAPSHOT",
  scalaVersion := "2.11.6",
  scalacOptions := Seq(
	"-feature",
	"-language:_",
	"-unchecked",
	"-deprecation",
	"-Ywarn-dead-code",
	"-Ywarn-unused",
	"-Ywarn-unused-import",
	"-target:jvm-1.8",
	"-Xlint:_",
	"-encoding",
	"utf8"
  )
)

val commonDependencies = Seq(
  "com.typesafe" % "config" % "1.2.1",
  "com.github.nscala-time" %% "nscala-time" % "1.8.0",
  "org.scalactic" %% "scalactic" % "2.2.4",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0",
  "ch.qos.logback" % "logback-classic" % "1.1.3",
  "org.scalatest" %% "scalatest" % "2.2.4" % Test,
  "org.scalacheck" %% "scalacheck" % "1.12.2" % Test
)

lazy val root = (project in file(".")).
	aggregate(common, notifications)

lazy val common = (project in file("common")).
	settings(commonSettings: _*).
	settings(libraryDependencies ++= commonDependencies).
  settings(libraryDependencies ++= Seq(
    "commons-codec" % "commons-codec" % "1.10"
  ))

lazy val notifications = (project in file("notifications")).
	settings(commonSettings: _*).
	settings(libraryDependencies ++= commonDependencies).
  settings(libraryDependencies ++= Seq(
    "com.typesafe.akka" %% "akka-actor" % "2.3.9" exclude ("org.scala-lang" , "scala-library"),
    "io.spray" %% "spray-json" % "1.3.1",
    "io.spray" %% "spray-client" % "1.3.3"
  )).
	dependsOn(common)