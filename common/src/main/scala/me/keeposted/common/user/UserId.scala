package me.keeposted.common.user

import java.util.UUID

/** A value class encapsulating a user id.
  *
  * A class encapsulating a string representation of the user id.
  * The string representation must conform to UUID representation
  * as described in the [[java.util.UUID.toString()]] method.
  */
case class UserId(value: String) {
  UUID.fromString(value)
}


object UserId {

  /** Generate a new user id
    *
    * @return
    */
  def generateNew: UserId = UserId(UUID.randomUUID().toString)
}