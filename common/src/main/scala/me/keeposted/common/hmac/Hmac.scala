package me.keeposted.common.hmac

import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

import org.apache.commons.codec.binary.Hex

object Hmac {

  def sha256: Encoder = HmacSha256

  trait Encoder {

    def algorithmName: String

    /** Compute the keyed hash message authentication code.
      * @return the HMAC result encoded in a hex string
      */
    def encode(key: String, data: String, charset: String = "UTF-8"): String

    /** Compute the keyed hash message authentication code. */
    def encode(key: Array[Byte], data: Array[Byte]): Array[Byte]

  }

  private case object HmacSha256 extends Encoder {
    override def algorithmName: String = "HmacSHA256"

    override def encode(key: String, data: String, charset: String = "UTF-8"): String = {
      Hex.encodeHexString(encode(key.getBytes(charset), data.getBytes(charset)))
    }

    override def encode(key: Array[Byte], data: Array[Byte]): Array[Byte] = {
      val mac = Mac.getInstance(algorithmName)
      mac.init(new SecretKeySpec(key, algorithmName))
      mac.doFinal(data)
    }
  }
}
